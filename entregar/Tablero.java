import java.util.ArrayList;

public class Tablero {
    ArrayList<Casilla> casillas = new ArrayList<Casilla>();

    public Tablero (){
	// agregar las 100 casillas al tablero
	for ( int i=0; i<=100; i++ ){
	    casillas.add ( new Casilla ( i ) );
	}
	// agregar las 7 escaleras al tablero
	casillas.get(5).agregarEscalera(24);
	casillas.get(15).agregarEscalera(47);
	casillas.get(50).agregarEscalera(71);
	casillas.get(20).agregarEscalera(81);
	casillas.get(71).agregarEscalera(89);
	casillas.get(39).agregarEscalera(75);
	casillas.get(42).agregarEscalera(100);

	// agregar las 7 serpientes
	casillas.get(99).agregarSerpiente(14);
	casillas.get(86).agregarSerpiente(25);
	casillas.get(73).agregarSerpiente(35);
	casillas.get(66).agregarSerpiente(46);
	casillas.get(54).agregarSerpiente(18);
	casillas.get(88).agregarSerpiente(9);
	casillas.get(40).agregarSerpiente(2);
    }

    public int regresarCasillas( int casillaActual, int casillas ){
	return casillaActual - casillas;
    }

    public int seguirEscalera (int casilla){
	// se ejecuta en el caso de que haya una escalera
	int casillaResultante = casillas.get(casilla).getEscalera();
	System.out.println("Encuentra una [Escalera] salta a la posicion [" + casillaResultante + " ]");
	return casillaResultante;
    }
    public int seguirSerpiente ( int casilla ){
	// se ejecuta en el caso de que hata una serpiente
	int casillaResultante = casillas.get(casilla).getSerpiente();
	System.out.println("Encuentra una [Serpiente] Retrocede a la posicion [ " + casillaResultante + " ]");
	return casillaResultante;
    }
    public int avanzarCasillas ( int casillaActual, int numero){
	// verificar que no se salga del tablero
	if ( casillaActual + numero > 100 ){
	    //se sobrepasa el tablero, se recorren los lugares
	    int suma = casillaActual + numero;
	    int diferencia = suma - 100;
	    
	    System.out.println("Se sale del tablero, se regresa [ " + diferencia + " ] casillas");
	    
	    return regresarCasillas ( casillaActual, diferencia );
	} 
	int casillaResultante = casillaActual + numero;

	System.out.println("Se mueve a la casilla [ " + casillaResultante + " ]");
	
	if ( casillas.get( casillaResultante ).getEscalera() != -1 ){
	    // hay una escalera
	    casillaResultante = seguirEscalera( casillaResultante );
	}else if ( casillas.get( casillaResultante ).getSerpiente() != -1 ){
	    // hay una serpiente
	    casillaResultante = seguirSerpiente( casillaResultante );
	}

	return casillaResultante;
    }
}
