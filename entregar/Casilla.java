public class Casilla {
    private int numero;
    private int serpiente;
    private int escalera;

    public Casilla ( int numero ){
	this.numero = numero;
	this.serpiente = -1;
	this.escalera = -1;
    }
    public int getNumero(){
	return this.numero;
    }
    
    public void agregarSerpiente ( int serpiente ){
	this.serpiente = serpiente;
    }
    public void agregarEscalera ( int escalera ){
	this.escalera = escalera;
    }
    public int getSerpiente (){
	return this.serpiente;
    }
    public int getEscalera (){
	return this.escalera;
    }
}
