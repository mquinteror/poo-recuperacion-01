public class Dado {
    private int numero;

    // setters y getters para numero se omiten, debido a
    // que ese atributo es generado por un metodo y no
    // necesita ser ingresado de manera externa

    public int generarTirada (){
	numero = (int) (Math.random()*12) +1;
	// prueba
	System.out.println("el resultado es " + numero);
	return numero;
    }
}
