public class Jugador {
    private String nombre;
    private int casillaActual;
    private int turno;
    private Dado dado;
    private Tablero tablero;
    boolean primerSalida;

    public Jugador ( int turno, String nombre ){
	this.turno = turno;
	this.nombre = nombre;
	this.casillaActual = 0;
	this.dado = new Dado();
	this.tablero = new Tablero();
	primerSalida = true;
    }

    public void tomarCasillaIndicada ( int casilla ){
	this.casillaActual = casilla;
    }

    public int getCasillaActual (){
	return this.casillaActual;
    }
    public String getNombre(){
	return this.nombre;
    }

    public void tirarDados (){
	// este metodo desencadena
	int resultado = dado.generarTirada();
	if ( primerSalida == true && resultado != 4 ){
	    System.out.println("La tirada del susuario [ "+ this.nombre + " ] es [" + resultado + " ] , sigue en la posicion inicial");
	} else {
	    primerSalida = false;
	    System.out.println("El jugador [ " + this.nombre + " ] obtubo la tirada:  [ " + resultado + " ]");
	    int casillaFinal = tablero.avanzarCasillas( this.casillaActual, resultado );
	    System.out.println("El uauario [ " + this.nombre + " ] esta en la casilla [" + casillaFinal + " ]");
	    tomarCasillaIndicada(casillaFinal);
	}
	
    }
}
