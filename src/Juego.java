import java.util.ArrayList;
import java.util.Scanner;

public class Juego {
    ArrayList<Jugador> jugadores;

    public Juego (){
	jugadores = new ArrayList<Jugador>();
    }

    public void bienvenida (){
	System.out.println("--------------------------------------------------------");
	System.out.println("-----   Bienvenbidos a serpientes y escaleras   --------");
	System.out.println("--------------------------------------------------------");
    }

    public void capturarJugadores (){
	String nombreJugador;
	String respuesta;
	int turnoJugador=1;
	Scanner sc = new Scanner(System.in);
	do {
	    System.out.println("Ingrsese nombre de jugador");
	    nombreJugador = sc.nextLine();
	    jugadores.add( new Jugador ( turnoJugador, nombreJugador ) );
	    System.out.println("agregar mas judadores [si/no]");
	    respuesta = sc.nextLine();
	    turnoJugador += 1;
	}while ( respuesta.equals("si") && turnoJugador <= 5);
    }

    // manejar los turnos
    public boolean manejarTurno (){
	for ( int i = 0; i< jugadores.size(); i++ ){
	    System.out.println("El turno es del jugador [ "+ jugadores.get(i).getNombre() + " ]");
	    jugadores.get(i).tirarDados();
	    int casillaActual = jugadores.get(i).getCasillaActual();
	    if ( casillaActual == 100 ){
		// el jugador actual es el anador
		System.out.println("El jugador [ " + jugadores.get(i).getNombre() + " ] es el gandador" );
		return true;
	    }
	}
	return false;
    }

    public void comenzarJuego (){
	while ( manejarTurno() != true ){
	    
	}
    }
   
}
